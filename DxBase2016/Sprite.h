#pragma once

#include "stdafx.h"

namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	class TitleSprite : public GameObject;
	//	用途: 配置オブジェクト
	//--------------------------------------------------------------------------------------
	class TitleSprite : public GameObject{
		Vector3 m_StartPos;
	public:
		//構築と破棄
		TitleSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~TitleSprite();
		//初期化
		virtual void OnCreate() override;
	};

	//--------------------------------------------------------------------------------------
	//	class MenuSprite : public GameObject;
	//	用途: 配置オブジェクト
	//--------------------------------------------------------------------------------------
	class MenuSprite : public GameObject{
		Vector3 m_StartPos;
	public:
		//構築と破棄
		MenuSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~MenuSprite();
		//初期化
		virtual void OnCreate() override;
	};
}