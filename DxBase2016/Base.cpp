#include "stdafx.h"
#include "Project.h"

namespace basedx11{

	//--------------------------------------------------------------------------------------
	//	class WhiteBase : public GameObject;
	//	用途: プレイヤーの拠点
	//--------------------------------------------------------------------------------------
	//構築と破棄
	WhiteBase::WhiteBase(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr),
		m_StartPos(StartPos),
		m_Span(3.0f)
	{
	}
	WhiteBase::~WhiteBase(){}

	//初期化
	void WhiteBase::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetScale(0.5f, 1.0f, 0.5f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		PtrTransform->SetPosition(m_StartPos);

		//反発をつける
		auto PtrRegid = AddComponent<Rigidbody>();

		//衝突判定をつける
		auto col = AddComponent <CollisionObb> ();
		col->SetFixed(false);
		auto PtrGravty = AddComponent<Gravity>();
		PtrGravty->SetBaseY(0.125f);

		//アライブを無効
		/*auto PtrArrive = AddComponent<ArriveSteering>();
		PtrArrive->SetUpdateActive(false);*/


		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(L"SIRO_TX");
	}

	//ベースの状態の処理
	//bool WhiteBase::BaseState(){
	//	auto PtrWhiteBase = GetComponent<CollisionObb>();
	//	return true;
	//}
	//void WhiteBase::StateChange(){
	//	auto PtrDraw = AddComponent<PNTStaticDraw>();
	//	PtrDraw->SetTextureResource(L"KURO_TX");
	//}
	////衝突判定
	//void WhiteBase::OnCollision(const shared_ptr<GameObject>& other){
	//	auto Ptrdraw = AddComponent<PNTStaticDraw>();
	//	Ptrdraw->SetTextureResource(L"KURO_TX");
	//}
	//更新

	//何かに衝突したときの処理
	//void WhiteBase::ObjectHitMotion()
	//{
	//	auto PtrCollision = GetComponent<CollisionObb>();
	//	//衝突判定
	//	if (PtrCollision->GetHitObject())
	//	{
	//		auto PtrSeekObj = dynamic_pointer_cast<SeekObject>(PtrCollision->GetHitObject());
	//		//相手のトランスフォームを取得
	//		auto SeakOtherTrans = PtrCollision->GetHitObject()->GetComponent<Transform>();
	//		//相手の座標を取得
	//		auto SeakOtherPos = SeakOtherTrans->GetPosition();
	//		//自分のトランスフォームを取得
	//		auto MyTrans = GetComponent<Transform>();
	//		//自分の座標を取得
	//		auto MyPos = MyTrans->GetPosition();

	//		//飛ぶ方向の計算
	//		MyPos += SeakOtherPos;
	//		MyPos.Normalize();
	//		MyPos.y = 0.0f;
	//		MyPos *= 1.0f;

	//		//衝突をなしにする
	//		PtrCollision->ClearBothHitObject();
	//		PtrCollision->SetUpdateActive(false);
	//		

	//	}
	//}

	void WhiteBase::OnLastUpdate(){
		//auto PtrTrans = GetComponent<Transform>();
		//auto ptrPos = PtrTrans->GetPosition();
		//ptrPos.y = m_StartPos.y;
		//PtrTrans->SetPosition(ptrPos);
	}

	//--------------------------------------------------------------------------------------
	//	class BlackBase : public GameObject;
	//	用途: プレイヤーの拠点
	//--------------------------------------------------------------------------------------
	//構築と破棄
	BlackBase::BlackBase(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr),
		m_StartPos(StartPos),
		m_Span(3.0f)
	{
	}
	BlackBase::~BlackBase(){}

	//初期化
	void BlackBase::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetScale(0.5f, 1.0f, 0.5f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		PtrTransform->SetPosition(m_StartPos);

		//反発をつける
		auto PtrRegid = AddComponent<Rigidbody>();

		//衝突判定をつける
		auto col = AddComponent <CollisionObb>();
		//col->SetFixed(true);

		//アライブを無効にする
		/*auto PtrArrive = AddComponent<ArriveSteering>();
		PtrArrive->SetUpdateActive(false);*/

		auto PtrGravty = AddComponent<Gravity>();
		PtrGravty->SetBaseY(0.125f);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(L"KURO_TX");
	}

	//更新
	void BlackBase::OnLastUpdate(){
		//auto PtrTrans = GetComponent<Transform>();
		//auto ptrPos = PtrTrans->GetPosition();
		//ptrPos.y = m_StartPos.y;
		//PtrTrans->SetPosition(ptrPos);
	}

}
//endof  basedx11