#pragma once

#include "stdafx.h"

namespace basedx11{
	


	//--------------------------------------------------------------------------------------
	//	class Switch : public GameObject;
	//	用途: スイッチの作成
	//--------------------------------------------------------------------------------------
	class Switch : public GameObject{
		//ステートマシン
		shared_ptr< StateMachine<Switch> >  m_StateMachine;	//ステートマシーン
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
		bool m_Switch;
		//インターバルのオン・オフ
		bool onoff;
		float count;
	public:
		//構築と破棄
		Switch(const shared_ptr<Stage>& StagePtr,
			const Vector3& Position,
			const Vector3& Rotation,
			const Vector3& Scale
			);

		virtual ~Switch();

		//アクセサ
		shared_ptr< StateMachine<Switch> > GetStateMachine() const{
			return m_StateMachine;
		}

		bool HitMotion();

		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
		virtual void OnLastUpdate() override;

		//操作
		virtual void OnCollision(const shared_ptr<GameObject>& other)override;
		
		//プレイヤーと衝突したときの処理
		void PlayerHitMotion();
		//スイッチの切り替え
		void SwitchChange();
		//スイッチのテクスチャの張替
		void SwitchSetTX();
	};


	//--------------------------------------------------------------------------------------
	//	class AState : public ObjState<Player>;
	//	用途: 押されていない状態
	//--------------------------------------------------------------------------------------
	class AState : public ObjState<Switch>
	{
		AState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<AState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Switch>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Switch>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Switch>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class BState : public ObjState<Player>;
	//	用途: 押された状態
	//--------------------------------------------------------------------------------------
	class BState : public ObjState<Switch>
	{
		BState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<BState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Switch>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Switch>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Switch>& Obj)override;
	};
	


}