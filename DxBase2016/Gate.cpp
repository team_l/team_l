#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	
	//--------------------------------------------------------------------------------------
	//	class Gate : public GameObject;
	//	用途: 門の作成
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Gate::Gate(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position,
		bool gateFlg
		) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position),
		GateFlg(gateFlg),
		Top_Position(Position.x,4.0f,Position.z),
		Bottom_Position(Position.x,0.0f,Position.z)

	{
	}
	Gate::~Gate(){}

	//初期化
	void Gate::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//反発を実装する場合はRigidbodyをつける
		auto PtrRegid = AddComponent<Rigidbody>();
		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);
		//AddComponent<PNTCollisionDraw>();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(L"KABE_TX");
		PtrDraw->SetOwnShadowActive(true);
		/*auto Group = GetStage()->GetSharedObjectGroup(L"GateGroup");
		Group->IntoGroup(GetThis<Gate>());*/
		//イベントマシンの構築
		m_EventMachine = make_shared<EventMachine<Gate>>(GetThis<Gate>());
		
		//イベントマシンとイベントステートを結びつける
		
		m_EventMachine->AddEventState(L"GateClose", SwitchChangeEvent::Instance());
		
		
		m_EventMachine->AddEventState(L"GateOpen", GateUpEvent::Instance());
		
	}

	void Gate::OnEvent(const shared_ptr<Event>& event)
	{
		//ハンドラ関数呼び出し
		m_EventMachine->HandleEvent(event);
		
	}

	//門を閉める
	void Gate::GateClose()
	{
		//ゲートのポジションを得る
		auto Trans = AddComponent<Transform>();
		auto Pos=Trans->GetPosition();
		
		Pos.y -= 0.08f;
		
		//ゲートが下限まで下がったら止める
		if (Pos.y < Bottom_Position.y){
			/*Trans->SetPosition(Bottom_Position);*/
			GateFlg = false;
			return;
		}

		Trans->SetPosition(Pos);

	}

	void Gate::Gateup()
	{
		//ゲートのポジションを得る
		auto Trans = AddComponent<Transform>();
		auto Pos = Trans->GetPosition();
		Pos.y += 0.08f;
		if (Pos.y > Top_Position.y){
			GateFlg = true;
			return;
	}
		Trans->SetPosition(Pos);
	}

	

	//スイッチがプレイヤーに当たったときのイベント
	  //イベントステートのインスタンスを得る
	shared_ptr<SwitchChangeEvent> SwitchChangeEvent::Instance()
	{
		static shared_ptr<SwitchChangeEvent> instance;
		if (!instance)
		{
			instance = shared_ptr<SwitchChangeEvent>(new SwitchChangeEvent);
		}
		return instance;
	}
	shared_ptr<GateUpEvent>GateUpEvent::Instance()
	{
		static shared_ptr<GateUpEvent> instance;
		if (!instance)
		{
			instance = shared_ptr<GateUpEvent>(new GateUpEvent);
		}
		return instance;
	}

	//このイベントが発生した時呼ばれる
	void SwitchChangeEvent::Enter(const shared_ptr<Gate>& Obj, const shared_ptr<Event>& event){
		Obj->GateClose();
	}
	void GateUpEvent::Enter(const shared_ptr<Gate>& Obj, const shared_ptr<Event>& event)
	{
		Obj->Gateup();
	}

	
}