#include "stdafx.h"
#include "Project.h"


namespace basedx11{

	//--------------------------------------------------------------------------------------
	//	class TitleStage : public Stage;
	//	用途: タイトルステージクラス
	//--------------------------------------------------------------------------------------
	//リソースの作成
	void TitleStage::CreateResourses(){
		//テクスチャー
		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"trace.png";
		App::GetApp()->RegisterTexture(L"TRACE_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"sky.jpg";
		App::GetApp()->RegisterTexture(L"SKY_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"wall.jpg";
		App::GetApp()->RegisterTexture(L"WALL_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"mizu.jpg";
		App::GetApp()->RegisterTexture(L"MIZU_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"siro.png";
		App::GetApp()->RegisterTexture(L"SIRO_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"kuro.png";
		App::GetApp()->RegisterTexture(L"KURO_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Title.jpg";
		App::GetApp()->RegisterTexture(L"TITLE_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Menu.jpg";
		App::GetApp()->RegisterTexture(L"MENU_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"renga.jpg";
		App::GetApp()->RegisterTexture(L"RENGA_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"koori.jpg";
		App::GetApp()->RegisterTexture(L"KOORI_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"ao.jpg";
		App::GetApp()->RegisterTexture(L"AO_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"kiiro.jpg";
		App::GetApp()->RegisterTexture(L"KIIRO_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"red.png";
		App::GetApp()->RegisterTexture(L"RED_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"kabe.jpg";
		App::GetApp()->RegisterTexture(L"KABE_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"umi.jpg";
		App::GetApp()->RegisterTexture(L"UMI_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Dirt.png";
		App::GetApp()->RegisterTexture(L"DIRT_TX", strTexture);

		//BGM
		wstring Titlewav = App::GetApp()->m_wstrRelativeDataPath + L"Title.wav";
		App::GetApp()->RegisterWav(L"Title", Titlewav);
		wstring Menuwav = App::GetApp()->m_wstrRelativeDataPath + L"Menu.wav";
		App::GetApp()->RegisterWav(L"Menu", Menuwav);
		wstring Mainwav = App::GetApp()->m_wstrRelativeDataPath + L"Main.wav";
		App::GetApp()->RegisterWav(L"Main", Mainwav);

		//SE
		wstring Switchwav = App::GetApp()->m_wstrRelativeDataPath + L"Switch.wav";
		App::GetApp()->RegisterWav(L"Switch", Switchwav);
		wstring Gatewav = App::GetApp()->m_wstrRelativeDataPath + L"Gate.wav";
		App::GetApp()->RegisterWav(L"Gate", Gatewav);
		wstring Shotwav = App::GetApp()->m_wstrRelativeDataPath + L"Shot.wav";
		App::GetApp()->RegisterWav(L"Shot", Shotwav);
		wstring Jumpwav = App::GetApp()->m_wstrRelativeDataPath + L"Jump.wav";
		App::GetApp()->RegisterWav(L"Jump", Jumpwav);

	}

	//ビューの作成
	void TitleStage::CreateViews(){
		//最初にデフォルトのレンダリングターゲット類を作成する
		//パラメータの2048.0fはシャドウマップのサイズ。大きいほど影が細かくなる（4096.0fなど）
		//影を細かくするとメモリを大量に消費するので注意！
		CreateDefaultRenderTargets(2048.0f);
		//影のビューサイズの設定。この値を小さくすると影が表示される範囲が小さくなる。
		//値が小さいほうが影は細かくなる
		//スタティック変数なので一度設定したらその値はステージを超えて保持される。
		Shadowmap::SetViewSize(32.0f);
		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();
		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();
		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());
		//ビューの背景色
		Color4 ViewBkColor(0.0f, 0.125f, 0.3f, 1.0f);
		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<Camera, MultiLight>(rect, ViewBkColor, 1, 0.0f, 1.0f);
		//最初のビューのビューのライトの設定
		auto PtrLight = PtrView->GetMultiLight()->GetLight(0);
		PtrLight->SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
		//ビューのカメラの設定
		auto PtrCamera = PtrView->GetCamera();
		PtrCamera->SetEye(Vector3(0.0f, 0.0f, -5.0f));
		PtrCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
	}
	     
		//ミュージックの再生
		void TitleStage::AddTitleMusic(){
			auto pMusic = AddComponent<PlayMusic>(L"Title");
			pMusic->Start(XAUDIO2_LOOP_INFINITE, 0.3f);
	}

	//文字列の作成
	void TitleStage::CreateString(){
		////文字列をつける
		//auto PtrString = AddComponent<StringSprite>();
		//PtrString->SetText(L"Bボタンでゲームを開始します");
		//Rect2D<float> rect(0, 0, 200, 200);
		//rect += Point2D<float>(550, 500);
		//PtrString->SetTextRect(rect);
	}

	//タイトルの作成
	void TitleStage::CreateTitle(){
		//配列の初期化
		vector<Vector3> Vec = {
			{ 0.0f, 200.0f, 0.0f },
		};
		//配置オブジェクトの作成
		for (auto v : Vec){
			AddGameObject<TitleSprite>(v);
		}
	}

	//初期化
	void TitleStage::OnCreate(){
		CreateResourses();
		CreateViews();
		CreateString();
		CreateTitle();
		AddTitleMusic();
	}

	//操作
	void TitleStage::OnUpdate(){
		//コントローラ情報の取得.
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		//Bボタンが押されたら.
		if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_B) {
			//イベント送出
			PostEvent(0.0f, GetThis<TitleStage>(), App::GetApp()->GetSceneBase(), L"ToMenu");
			//ミュージックをストップ
			auto pMusic = AddComponent<PlayMusic>(L"Title");
			pMusic->Stop();

		}
	}

	//--------------------------------------------------------------------------------------
	//	class MenuStage : public Stage;
	//	用途: メニューステージクラス
	//--------------------------------------------------------------------------------------
	//ビューの作成
	void MenuStage::CreateViews(){
		//最初にデフォルトのレンダリングターゲット類を作成する
		//パラメータの2048.0fはシャドウマップのサイズ。大きいほど影が細かくなる（4096.0fなど）
		//影を細かくするとメモリを大量に消費するので注意！
		CreateDefaultRenderTargets(2048.0f);
		//影のビューサイズの設定。この値を小さくすると影が表示される範囲が小さくなる。
		//値が小さいほうが影は細かくなる
		//スタティック変数なので一度設定したらその値はステージを超えて保持される。
		Shadowmap::SetViewSize(32.0f);
		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();
		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();
		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());
		//ビューの背景色
		Color4 ViewBkColor(0.0f, 0.125f, 0.3f, 1.0f);
		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<Camera, MultiLight>(rect, ViewBkColor, 1, 0.0f, 1.0f);
		//最初のビューのビューのライトの設定
		auto PtrLight = PtrView->GetMultiLight()->GetLight(0);
		PtrLight->SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
		//ビューのカメラの設定
		auto PtrCamera = PtrView->GetCamera();
		PtrCamera->SetEye(Vector3(0.0f, 0.0f, -5.0f));
		PtrCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
	}

	//文字列の作成
	void MenuStage::CreateString(){
		////文字列をつける
		//auto PtrString = AddComponent<StringSprite>();
		//PtrString->SetText(L"Bボタンでゲームスタート　　　　　Aボタンでタイトルに戻ります");
		//Rect2D<float> rect(0, 0, 200, 200);
		//rect += Point2D<float>(550, 500);
		//PtrString->SetTextRect(rect);
	}

	//タイトルの作成
	void MenuStage::CreateTitle(){
		//配列の初期化
		vector<Vector3> Vec = {
			{ 0.0f, 200.0f, 0.0f },
		};
		//配置オブジェクトの作成
		for (auto v : Vec){
			AddGameObject<MenuSprite>(v);
		}
	}

	//ミュージックの再生
	void MenuStage::AddMusic(){
			auto pMusic = AddComponent<PlayMusic>(L"Menu");
			pMusic->Start(XAUDIO2_LOOP_INFINITE, 0.3f);
		
	}
	

	//初期化
	void MenuStage::OnCreate(){
		CreateViews();
		CreateString();
		CreateTitle();
		AddMusic();

	}

	//操作
	void MenuStage::OnUpdate(){
		//コントローラ情報の取得.
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		//Bボタンが押されたら.
		if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
			//イベント送出
			PostEvent(0.0f, GetThis<MenuStage>(), App::GetApp()->GetSceneBase(), L"ToTitle");
			//ミュージックをストップ
			auto pMusic = AddComponent<PlayMusic>(L"Menu");
			pMusic->Stop();
		}
		if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_B){
			//イベント送出
			PostEvent(0.0f, GetThis<MenuStage>(), App::GetApp()->GetSceneBase(), L"ToGame");
			//ミュージックをストップ
			auto pMusic = AddComponent<PlayMusic>(L"Menu");
			pMusic->Stop();

		}
	}

	//--------------------------------------------------------------------------------------
	//	class GameStage : public Stage;
	//	用途: ゲームステージクラス
	//--------------------------------------------------------------------------------------
	//ビュー類の作成
	void GameStage::CreateViews(){
		//最初にデフォルトのレンダリングターゲット類を作成する
		//パラメータの2048.0fはシャドウマップのサイズ。大きいほど影が細かくなる（4096.0fなど）
		//影を細かくするとメモリを大量に消費するので注意！
		CreateDefaultRenderTargets(2048.0f);
		//影のビューサイズの設定。この値を小さくすると影が表示される範囲が小さくなる。
		//値が小さいほうが影は細かくなる
		//スタティック変数なので一度設定したらその値はステージを超えて保持される。
		Shadowmap::SetViewSize(32.0f);
		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();
		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();
		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());
		//ビューの背景色
		Color4 ViewBkColor(0.0f, 0.125f, 0.3f, 1.0f);
		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<LookAtCamera, MultiLight>(rect, ViewBkColor, 1, 0.0f, 1.0f);
		//最初のビューのビューのライトの設定
		auto PtrLight = PtrView->GetMultiLight()->GetLight(0);
		PtrLight->SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
		//ビューのカメラの設定
		auto PtrCamera = PtrView->GetCamera();
		PtrCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		PtrCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
	}


	//プレートの作成
	void GameStage::CreatePlate(){
		//ステージへのゲームオブジェクトの追加
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTransMatrix = Ptr->GetComponent<TransformMatrix>();
		Quaternion Qt;
		Qt.RotationRollPitchYawFromVector(Vector3(XM_PIDIV2, 0, 0));
		Matrix4X4 WorldMat;
		WorldMat.DefTransformation(
			Vector3(200.0f, 200.0f, 1.0f),
			Qt,
			Vector3(0.0f, 0.0f, 0.0f)
			);
		PtrTransMatrix->SetWorldMatrix(WorldMat);
		
		//描画コンポーネントの追加
		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();
		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");
		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"SKY_TX");
		DrawComp->SetTextureResource(L"UMI_TX");

		//自分に影が映りこむようにする
		DrawComp->SetOwnShadowActive(true);

		
	}

	//床の作成
	void GameStage::CreateFloor(){
		//配列の初期化
		vector< vector<Vector3> > Vec = {
			{
				Vector3(13.0f, 4.0f, 6.5f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(0.0f, -0.9f, 9.0f)

			},
			{
				Vector3(13.0f, 4.0f, 6.5f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(0.0f, -0.9f, 0.0f)

			},
			{
				Vector3(3.75f, 4.0f, 2.5f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(0.0f, -0.9f, 4.5f)
			 }

		};
		//オブジェクトの作成
		for (auto v : Vec){
				AddGameObject<Floor>(v[0], v[1], v[2]);
		}
	}

	//川の作成
	void GameStage::CreateRiver(){
		//配列の初期化
		vector< vector<Vector3> > Vec = {
			{
				Vector3(19.5f, 3.0f, 21.0f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(0.0f, -0.9f, 4.6875f)
			}
		};
		//オブジェクトの作成
		for (auto v : Vec){
			AddGameObject<River>(v[0], v[1], v[2]);
		}
	}

	//壁の作成
	void GameStage::CreateWall(){
		//配列の初期化
		vector< vector<Vector3> > Vec = {
			{
				Vector3(0.5f, 9.0f, 21.0f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(-9.88f, -0.9f, 4.6875f)
			},
			{
				Vector3(0.5f, 9.0f, 21.0f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(9.88f, -0.9f, 4.6875f)
			},
			{
				Vector3(20.25f, 9.0f, 0.5f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(0.0f, -0.9f, 15.44f)

			},
			{
				Vector3(20.25f, 9.0f, 0.5f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(0.0f, -0.9f, -6.055f)

			}
		};
		//オブジェクトの作成
		for (auto v : Vec){
			AddGameObject<Wall>(v[0], v[1], v[2]);
		}
	}

	//浄化ポイントの作成
	void GameStage::CreatePurificationPoint(){
		//配列の初期化
		vector< vector<Vector3> > Vec = {
			
			{

				Vector3(2.0f, 4.0f, 2.6f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(5.25f, -0.9f, -4.55f)
			}
			
		};

		//オブジェクトの作成
		for (auto v : Vec){
			AddGameObject<PurificationPoint>(v[0], v[1], v[2]);

		}
	}

	void GameStage::CreateEnemyPurficationPoint()
	{
		AddGameObject<EnemyPurificationPoint>
			(
			Vector3(2.0f, 4.0f, 2.85f),
			Vector3(0.0f, 0.0f, 0.0f),
			Vector3(-5.25, -0.9f, 13.7f)
			);

	}

	//門の作成
	void GameStage::CreateGate(){
		//配列の初期化
		/*vector< vector<Vector3> > Vec = {
			{
				Vector3(3.0f, 5.0f, 0.5f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(11.5f,4.0f, 7.25f)
			}
		};*/
		auto Ptr = AddGameObject<Gate>(
			Vector3(3.0f, 5.0f, 0.5f),
			Vector3(0.0f, 0.0f, 0.0f),
			Vector3(8.0f, 4.0f, 5.25f),
			true
			);
		Ptr = AddGameObject<Gate>(
			Vector3(3.0f, 5.0f, 0.5f),
			Vector3(0.0f, 0.0f, 0.0f),
			Vector3(-8.0f, 0.0f, 2.75f)
			,true
			);
		auto Group = CreateSharedObjectGroup(L"GateGroup");
		//オブジェクトの作成
		/*for (auto v : Vec){
			auto Ptr=AddGameObject<Gate>(v[0], v[1], v[2]);
			Group->IntoGroup(Ptr);
			SetSharedGameObject(L"Gate", Ptr);
		}*/
		Group->IntoGroup(Ptr);
		SetSharedGameObject(L"Gate", Ptr);

	}

	//スイッチの作成
	void GameStage::CreateSwitch(){
		/*//配列の初期化
		vector< vector<Vector3> > Vec = {
			{
				Vector3(9.9f, -0.9f, 7.25f),
			    
			},
			{
				Vector3(-9.87f, -0.9f, 4.7f)
			}
			
		};*/
		//スイッチオブジェクトの作成
		/*auto PtrSwitch = AddGameObject<Switch>(
			Vector3(9.5f, -0.9f, 7.6f),
			
			1
			);
		PtrSwitch = AddGameObject<Switch>(
			Vector3(-9.57f, -0.9f, 4.4f),
			
			2
			);*/
		vector< vector<Vector3> > Vec = {
			{
			
				Vector3(5.5f, 1.25f, 7.0f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(0.25f, 1.5f, 0.25f)


			},
			{
				Vector3(-5.5f, 1.25f, 2.0f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(0.25f, 1.5f, 0.25f)
			}
		};
		//スイッチのシェアオブジェクトグループの作成
		auto Group = CreateSharedObjectGroup(L"SwitchGroup");
		//オブジェクトの作成
		for (auto v : Vec){
			auto Ptr=AddGameObject<Switch>(v[0], v[1], v[2]);
			Group->IntoGroup(Ptr);
		}
		//オブジェクトをグループに追加
		//Group->IntoGroup(PtrSwitch);
	}

	//追いかけるオブジェクトの作成
	void GameStage::CreateSeekObject(){
		//オブジェクトのグループを作成する
		auto Group = CreateSharedObjectGroup(L"ObjectGroup");
		//配列の初期化
		vector<Vector3> Vec = {
			{ 1.0f, 2.0f, 5.0f },
			{ 2.0f, 2.0f, 6.0f },
			{ 2.0f, 2.0f, 6.0f },
			{ 1.0f, 2.0f, 5.0f },
		};
		//配置オブジェクトの作成
		for (auto v : Vec){
			AddGameObject<SeekObject>(v);
		}
	}

	//プレイヤーの作成
	void GameStage::CreatePlayer(){
		//プレーヤーの作成
		auto PlayerPtr = AddGameObject<Player>();
		//シェア配列にプレイヤーを追加
		SetSharedGameObject(L"Player", PlayerPtr);
	}

	//味方の作成
	void GameStage::CreateSupporter(){
		//オブジェクトのグループを作成する
		auto Group = CreateSharedObjectGroup(L"SupporterGroup");
		//配列の初期化
		vector<Vector3> Vec = {
			{ 1.0f, 1.5f, 0.0f },
			{ 0.5f, 1.5f, 1.0f },
			{ 0.5f, 1.5f, 1.0f },
			{ 1.0f, 1.5f, 0.0f },
		};
		//配置オブジェクトの作成
		for (auto v : Vec){
			AddGameObject<Supporter>(v);
		}

	}

	//防衛拠点の作成
	void GameStage::CreateBase(){
		auto WhiteBasePtr = AddGameObject<WhiteBase>(Vector3(0, 1.6f, -2.25f));//0.5,3
		SetSharedGameObject(L"WhiteBase", WhiteBasePtr);
		AddGameObject<BlackBase>(Vector3(0, 1.6f, 11.25f));//0.5,15
	}

	//ミュージックの再生
	void GameStage::AddGameMusic(){
		auto pMusic = AddComponent<PlayMusic>(L"Main");
		pMusic->Start(XAUDIO2_LOOP_INFINITE, 0.3f);
	}

	//初期化
	void GameStage::OnCreate(){
		try{
			//ビュー類を作成する
			CreateViews();
			//プレートを作成する
			CreatePlate();
			//床の作成
			CreateFloor();
			//川の作成
			CreateRiver();
			//壁の作成
			CreateWall();
			//浄化ポイントの作成
			CreatePurificationPoint();
			//敵の浄化ポイントの作成
			CreateEnemyPurficationPoint();
			//門の作成
			CreateGate();
			//スイッチの作成
			CreateSwitch();
			//プレイヤーの作成
			CreatePlayer();
			//味方の作成
			//CreateSupporter();
			//防衛拠点の作成
			CreateBase();
			//追いかけるオブジェクト	
			CreateSeekObject();
			//砲弾用のシェアオブジェクトグループの作成
			CreateSharedObjectGroup(L"ShellBallGroup");
			//ミュージックの再生
			AddGameMusic();
			

		}
		catch (...){
			throw;
		}
	}




}
//endof  basedx11
