#pragma once

#include "stdafx.h"

namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	class Gate : public GameObject;
	//	用途: 門の作成
	//--------------------------------------------------------------------------------------
	class Gate : public GameObject{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
		Vector3 Top_Position;//門が上がるときの上限
		Vector3 Bottom_Position;//門が下がるときの下限
		bool GateFlg;
		//イベントマシーン
		shared_ptr< EventMachine<Gate> >  m_EventMachine;
		
		
	public:
		//構築と破棄
		Gate(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position,
			bool GateFlag
			);
		virtual ~Gate();
		//初期化
		virtual void OnCreate() override;

		//イベントのハンドラ
		virtual void OnEvent(const shared_ptr<Event>& event)override;

		//門の開け閉め
		void GateClose();
		void Gateup();
	};

	//--------------------------------------------------------------------------------------
	//class SwitchChangeEvent :public GameObject;
	//用途:  スイッチがプレイヤーに当たったときのイベント
	//--------------------------------------------------------------------------------------

	class SwitchChangeEvent :public EventState<Gate>{
		SwitchChangeEvent(){}
	public:
		//イベントステートのインスタンスを得る
		static shared_ptr<SwitchChangeEvent> Instance();
		//このイベントが発生した時呼ばれる
		virtual void Enter(const shared_ptr<Gate>&, const shared_ptr<Event>& event)override;


	};

	//----------------------------------------------
	//ゲートが上がるイベント
	//
	class GateUpEvent : public EventState < Gate > {
		GateUpEvent(){}
	public:
		//イベントステートのインスタンスを得る
		static shared_ptr<GateUpEvent> Instance();
		//このイベントが発生した時呼ばれる
		virtual void Enter(const shared_ptr<Gate>&, const shared_ptr<Event>& event)override;
	};




	
}