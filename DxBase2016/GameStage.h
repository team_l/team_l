#pragma once

#include "stdafx.h"

namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	class TitleStage : public Stage;
	//	用途: タイトルステージクラス
	//--------------------------------------------------------------------------------------
	class TitleStage : public Stage{
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViews();
		//文字列の作成
		void CreateString();
		//タイトルの作成
		void CreateTitle();
		//ミュージックの再生
		void AddTitleMusic();

	public:
		//構築と破棄
		TitleStage() :Stage(){}
		virtual ~TitleStage(){}
		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate() override;

	};

	//--------------------------------------------------------------------------------------
	//	class MenuStage : public Stage;
	//	用途: メニューステージクラス
	//--------------------------------------------------------------------------------------
	class MenuStage : public Stage{
		//ビューの作成
		void CreateViews();
		//文字列の作成
		void CreateString();
		//メニューの作成
		void CreateTitle();
		//ミュージックの再生
		void AddMusic();

	public:
		//構築と破棄
		MenuStage() :Stage(){}
		virtual ~MenuStage(){}
		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate() override;

	};
	//--------------------------------------------------------------------------------------
	//	class GameStage : public Stage;
	//	用途: ゲームステージクラス
	//--------------------------------------------------------------------------------------
	class GameStage : public Stage{
		//ビューの作成
		void CreateViews();
		//プレートの作成
		void CreatePlate();
		//床の作成
		void CreateFloor();
		//川の作成
		void CreateRiver();
		//壁の作成
		void CreateWall();
		//浄化ポイントの作成
		void CreatePurificationPoint();
		void CreateEnemyPurficationPoint();
		//門の作成
		void CreateGate();
		//スイッチの作成
		void CreateSwitch();
		//味方の作成
		void CreateSupporter();
		//プレイヤーの作成
		void CreatePlayer();
		//防衛拠点の作成
		void CreateBase();
		//追いかけるオブジェクトの作成
		void CreateSeekObject();
		//ミュージックの再生
		void AddGameMusic();
		

	public:
		//構築と破棄
		GameStage() :Stage(){}
		virtual ~GameStage(){}
		//初期化
		virtual void OnCreate()override;
	};
}
//endof  basedx11
