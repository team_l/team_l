﻿#include "stdafx.h"
#include "Project.h"

namespace basedx11{

	//--------------------------------------------------------------------------------------
	//	class SeekObject : public GameObject;
	//	用途: 追いかける配置オブジェクト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	SeekObject::SeekObject(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr),
		m_StartPos(StartPos),
		m_BaseY(m_StartPos.y),
		m_StateChangeSize(5.0f)
	{
	}
	SeekObject::~SeekObject(){}

	//初期化
	void SeekObject::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(0.25f, 0.25f, 0.25f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//操舵系のコンポーネントをつける場合はRigidbodyをつける
		auto PtrRegid = AddComponent<Rigidbody>();
		auto PtrGravity = AddComponent<Gravity>();
		PtrGravity->SetBaseY(0.125f);

		//壁を作成（時計回りの3点で定義する面）
		vector<Plane> PlaneVec;
		PlaneVec.push_back(Plane(Vector3(0, 0, 20.0f), Vector3(0, 10.0f, 20.0f), Vector3(10.0f, 0, 20.0f)));
		PlaneVec.push_back(Plane(Vector3(-20.0f, 0, 0), Vector3(-20.0f, 10.0f, 0), Vector3(-20.0f, 0, 10.0f)));
		PlaneVec.push_back(Plane(Vector3(20.0f, 0, 0), Vector3(20.0f, 10.0f, 0), Vector3(20.0f, 0, -10.0f)));
		PlaneVec.push_back(Plane(Vector3(0, 0, -20.0f), Vector3(0, 10.0f, -20.0f), Vector3(-10.0f, 0, -20.0f)));
		//壁回避をつける
		auto WallAvoidancePtr = AddComponent<WallAvoidanceSteering>();
		WallAvoidancePtr->SetPlaneVec(PlaneVec);

		//Seek操舵
		auto PtrSeek = AddComponent<SeekSteering>();
		//Arrive操舵
		auto PtrArrive = AddComponent<ArriveSteering>();
		//Arriveは無効にしておく
		PtrArrive->SetUpdateActive(false);
		//オブジェクトのグループを得る
		auto Group = GetStage()->GetSharedObjectGroup(L"ObjectGroup");
		//グループに自分自身を追加
		Group->IntoGroup(GetThis<SeekObject>());
		//分離行動をつける
		//AddComponent<SeparationSteering>(Group);
		//💩

		//Obbの衝突判定をつける
		auto PtrColl = AddComponent<CollisionObb>();
		PtrColl->SetFixed(false);
		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(L"KURO_TX");
		//透明処理をする
		SetAlphaActive(true);

		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<SeekObject> >(GetThis<SeekObject>());
		//最初のステートをSeekFarStateに設定
		m_StateMachine->SetCurrentState(FarState::Instance());
		//初期化実行を行う
		m_StateMachine->GetCurrentState()->Enter(GetThis<SeekObject>());
	}

	//ユーティリティ関数群
	Vector3 SeekObject::GetPlayerPosition() const{
		//もし防衛陣が初期化化されてない場合には、Vector3(0,m_BaseY,0)を返す
		Vector3 WhiteBasePos(0, m_BaseY, 0);
		auto PtrWhiteBase = GetStage()->GetSharedGameObject<WhiteBase>(L"WhiteBase", false);
		if (PtrWhiteBase){
			WhiteBasePos = PtrWhiteBase->GetComponent<Transform>()->GetPosition();
			//WhiteBasePos.y = m_BaseY;
		}
		return WhiteBasePos;
	}
	float SeekObject::GetPlayerLength() const{
		auto MyPos = GetComponent<Transform>()->GetPosition();
		auto LenVec = GetPlayerPosition();
		return LenVec.Length();
	}

	//ユーティリティ関数
	
	//味方の防衛陣にぶつかったら
	void SeekObject::OnCollision(const shared_ptr<GameObject>& other)
	{
		 
	}

	//操舵を変更する
	void SeekObject::ChangeSteering(){
		//壁の奥にいっていたら、他の操舵は無効とする
		auto WallAvoidancePtr = GetComponent<WallAvoidanceSteering>();
		if (WallAvoidancePtr->IsWallArribed()){
			auto PtrSeek = GetComponent<SeekSteering>();
			PtrSeek->SetUpdateActive(false);
			auto PtrArrive = GetComponent<ArriveSteering>();
			PtrArrive->SetUpdateActive(false);
		}
		else{
			//奥にいってなかったら本来の操舵を戻す
			if (m_StateMachine->GetCurrentState() == FarState::Instance()){
				auto PtrSeek = GetComponent<SeekSteering>();
				PtrSeek->SetUpdateActive(true);
			}
			else if (m_StateMachine->GetCurrentState() == NearState::Instance()){
				auto PtrArrive = GetComponent<ArriveSteering>();
				PtrArrive->SetUpdateActive(true);
			}
		}
	}

	//モーションを実装する関数群
	void  SeekObject::SeekStartMoton(){
		auto PtrSeek = GetComponent<SeekSteering>();
		PtrSeek->SetUpdateActive(true);
		PtrSeek->SetTargetPosition(GetPlayerPosition());

	}
	bool  SeekObject::SeekUpdateMoton(){
		auto PtrSeek = GetComponent<SeekSteering>();
		PtrSeek->SetTargetPosition(GetPlayerPosition());
		if (GetPlayerLength() <= m_StateChangeSize){
			return true;
		}
		return false;
	}
	void  SeekObject::SeekEndMoton(){
		auto PtrSeek = GetComponent<SeekSteering>();
		PtrSeek->SetUpdateActive(false);
	}

	void  SeekObject::ArriveStartMoton(){
		auto PtrArrive = GetComponent<ArriveSteering>();
		PtrArrive->SetUpdateActive(true);
		PtrArrive->SetTargetPosition(GetPlayerPosition());
	}
	bool  SeekObject::ArriveUpdateMoton(){
		auto PtrArrive = GetComponent<ArriveSteering>();
		PtrArrive->SetTargetPosition(GetPlayerPosition());
		if (GetPlayerLength() > m_StateChangeSize){
			//プレイヤーとの距離が一定以上ならtrue
			return true;
		}
		return false;
	}
	void  SeekObject::ArriveEndMoton(){
		auto PtrArrive = GetComponent<ArriveSteering>();
		//Arriveコンポーネントを無効にする
		PtrArrive->SetUpdateActive(false);
	}

	void SeekObject::ShellHitMotion(){
		//衝突判定を得る
		auto PtrCollision = GetComponent<CollisionSphere>();
		//衝突した
		if (PtrCollision->GetHitObject()){
			auto ShellPtr = dynamic_pointer_cast<Waterbullet>(PtrCollision->GetHitObject());

			//相手のTransformを得る。
			auto PtrOtherTrans = PtrCollision->GetHitObject()->GetComponent<Transform>();
			//相手の場所を得る
			auto OtherPos = PtrOtherTrans->GetPosition();

			//Transformを得る。
			auto PtrTrans = GetComponent<Transform>();
			//場所を得る
			auto Pos = PtrTrans->GetPosition();

			//飛ぶ方向を計算する
			Pos += OtherPos;
			Pos.Normalize();
			Pos.y = 0;
			Pos *= 1.0f;
			Pos += Vector3(0, 1.0f, 0);

			//衝突をなしにする（のちに復活）
			PtrCollision->ClearBothHitObject();
			PtrCollision->SetUpdateActive(false);

			//重力を得る
			auto PtrGravity = GetComponent<Gravity>();
			//ジャンプスタート
			PtrGravity->StartJump(Pos);


		}

	}

	//落下終了したらtrueを返す
	bool SeekObject::ShellHitMoveMotion(){
		//重力を得る
		auto PtrGravity = GetComponent<Gravity>();
		if (PtrGravity->IsGravityVelocityZero()){
			//落下終了
			//衝突判定を得る
			auto PtrCollision = GetComponent<CollisionSphere>();
			//衝突を有効にする
			PtrCollision->SetUpdateActive(true);
			Quaternion Qt;
			Qt.Identity();
			GetComponent<Transform>()->SetQuaternion(Qt);

			return true;
		}
		auto Qt = GetComponent<Transform>()->GetQuaternion();
		Quaternion Span;
		Span.RotationRollPitchYawFromVector(Vector3(0.2f, 0.2f, 0.2f));
		Qt *= Span;
		GetComponent<Transform>()->SetQuaternion(Qt);
		return false;
	}

	//操作
	void SeekObject::OnUpdate(){
		//ステートマシンのUpdateを行う
		//この中でステートの切り替えが行われる
		m_StateMachine->Update();

		auto PtrRigid = GetComponent<Rigidbody>();
		auto Vec = PtrRigid->GetVelocity();
		Vec.y = 0.0f;
		PtrRigid->SetVelocity(Vec);
	}

	

	void SeekObject::OnLastUpdate(){
		//操舵を変更する
		ChangeSteering();
		auto PtrRigidbody = GetComponent<Rigidbody>();
		//回転の更新
		//Velocityの値で、回転を変更する
		//これで進行方向を向くようになる
		auto PtrTransform = GetComponent<Transform>();
		Vector3 Velocity = PtrRigidbody->GetVelocity();
		if (Velocity.Length() > 0.0f){
			Vector3 Temp = Velocity;
			Temp.Normalize();
			float ToAngle = atan2(Temp.x, Temp.z);
			Quaternion Qt;
			Qt.RotationRollPitchYaw(0, ToAngle, 0);
			Qt.Normalize();
			//現在の回転を取得
			Quaternion NowQt = PtrTransform->GetQuaternion();
			//現在と目標を補間（10分の1）
			NowQt.Slerp(NowQt, Qt, 0.1f);
			PtrTransform->SetQuaternion(NowQt);
		}
		////常にyはm_BaseY
		//auto Pos = PtrTransform->GetPosition();
		//Pos.y = m_BaseY;
		//PtrTransform->SetPosition(Pos);
	}
	//--------------------------------------------------------------------------------------
	//	class FarState : public ObjState<SeekObject>;
	//	用途: プレイヤーから遠いときの移動
	//--------------------------------------------------------------------------------------
	shared_ptr<FarState> FarState::Instance(){
		static shared_ptr<FarState> instance(new FarState);
		return instance;
	}
	void FarState::Enter(const shared_ptr<SeekObject>& Obj){
		Obj->SeekStartMoton();
	}
	void FarState::Execute(const shared_ptr<SeekObject>& Obj){
		if (Obj->SeekUpdateMoton()){
			Obj->GetStateMachine()->ChangeState(NearState::Instance());
		}
	}
	void FarState::Exit(const shared_ptr<SeekObject>& Obj){
		Obj->SeekEndMoton();
	}

	//--------------------------------------------------------------------------------------
	//	class NearState : public ObjState<SeekObject>;
	//	用途: プレイヤーから近いときの移動
	//--------------------------------------------------------------------------------------
	shared_ptr<NearState> NearState::Instance(){
		static shared_ptr<NearState> instance(new NearState);
		return instance;
	}
	void NearState::Enter(const shared_ptr<SeekObject>& Obj){
		Obj->ArriveStartMoton();
	}
	void NearState::Execute(const shared_ptr<SeekObject>& Obj){
		if (Obj->ArriveUpdateMoton()){
			Obj->GetStateMachine()->ChangeState(FarState::Instance());
		}
	}
	void NearState::Exit(const shared_ptr<SeekObject>& Obj){
		Obj->ArriveEndMoton();
	}

	//--------------------------------------------------------------------------------------
	//	class NearState : public ObjState<SeekObject>;
	//	用途: 弾に当たった時の処理
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<ShellHitState> ShellHitState::Instance(){
		static shared_ptr<ShellHitState> instance;
		if (!instance){
			instance = shared_ptr<ShellHitState>(new ShellHitState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void ShellHitState::Enter(const shared_ptr<SeekObject>& Obj){
		//衝突した瞬間の処理
		Obj->ShellHitMotion();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void ShellHitState::Execute(const shared_ptr<SeekObject>& Obj){
		if (Obj->ShellHitMoveMotion()){
			//落下終了ならステート変更
			Obj->GetStateMachine()->ChangeState(FarState::Instance());
		}
	}
	//ステートにから抜けるときに呼ばれる関数
	void ShellHitState::Exit(const shared_ptr<SeekObject>& Obj){
		//何もしない
	}

}
//endof  basedx11
