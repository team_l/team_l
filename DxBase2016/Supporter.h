#pragma once

#include "stdafx.h"

namespace basedx11{

	//--------------------------------------------------------------------------------------
	//	class Supporter : public GameObject;
	//	用途: 追いかける配置オブジェクト
	//--------------------------------------------------------------------------------------
	class Supporter : public GameObject{
		shared_ptr< StateMachine<Supporter> >  m_StateMachine;	//ステートマシーン
		Vector3 m_StartPos;
		float m_BaseY;
		float m_StateChangeSize;
		//ユーティリティ関数群
		//プレイヤーの位置を返す
		Vector3 GetEnemyPosition() const;
		//プレイヤーまでの距離を返す
		float GetEnemyLength() const;
		//操舵を変更する
		void ChangeSteering();
	public:
		//構築と破棄
		Supporter(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~Supporter();
		//初期化
		virtual void OnCreate() override;
		//アクセサ
		shared_ptr< StateMachine<Supporter> > GetStateMachine() const{
			return m_StateMachine;
		}
		//衝突判定
		virtual void OnCollision(const shared_ptr<GameObject>& other) override;
		//モーションを実装する関数群
		void  SeekStartMoton();
		bool  SeekUpdateMoton();
		void  SeekEndMoton();

		void  ArriveStartMoton();
		bool  ArriveUpdateMoton();
		void  ArriveEndMoton();

		void ShellHitMotion();

		bool ShellHitMoveMotion();

		//操作
		virtual void OnUpdate() override;
		//ターンの最終更新時
		virtual void OnLastUpdate() override;
	};

	//--------------------------------------------------------------------------------------
	//	class WaitState : public ObjState<SeekObject>;
	//	用途: プレイヤーから遠いときの移動
	//--------------------------------------------------------------------------------------
	class WaitState : public ObjState<Supporter>
	{
		WaitState(){}
	public:
		static shared_ptr<WaitState> Instance();
		virtual void Enter(const shared_ptr<Supporter>& Obj)override;
		virtual void Execute(const shared_ptr<Supporter>& Obj)override;
		virtual void Exit(const shared_ptr<Supporter>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class InterceptState : public ObjState<SeekObject>;
	//	用途: プレイヤーから近いときの移動
	//--------------------------------------------------------------------------------------
	class InterceptState : public ObjState<Supporter>
	{
		InterceptState(){}
	public:
		static shared_ptr<InterceptState> Instance();
		virtual void Enter(const shared_ptr<Supporter>& Obj)override;
		virtual void Execute(const shared_ptr<Supporter>& Obj)override;
		virtual void Exit(const shared_ptr<Supporter>& Obj)override;
	};
}