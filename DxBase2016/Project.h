#pragma once


#include "resource.h"
#include "Scene.h"
#include "GameStage.h"
#include "Character.h"
#include "Player.h"
#include "Enemy.h"
#include "Base.h"
#include "Sprite.h"
#include "Stage.h"
#include "Switch.h"
#include "Gate.h"
#include "PurificationPoint.h"
#include "River.h"
#include "Supporter.h"