#pragma once

#include "stdafx.h"

namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	class PurificationPoint : public GameObject;
	//	�p�r: �򉻃|�C���g�̍쐬
	//--------------------------------------------------------------------------------------
	class PurificationPoint : public GameObject{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//�\�z�Ɣj��
		PurificationPoint(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
			);
		virtual ~PurificationPoint();
		//������
		virtual void OnCreate() override;
		virtual void OnCollision(const shared_ptr<GameObject>& other)override;
		//����
	};
	
	//----------------------------------------------
	//�G�̏򉻃|�C���g
	//______________________________________________
	class EnemyPurificationPoint :public GameObject{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		EnemyPurificationPoint(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
			);
		virtual ~EnemyPurificationPoint();
		virtual void OnCreate() override;
		virtual void OnCollision(const shared_ptr<GameObject>& other)override;
	};
}