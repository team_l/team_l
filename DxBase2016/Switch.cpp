#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	class Switch : public GameObject;
	//	用途: スイッチの作成
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Switch::Switch(const shared_ptr<Stage>& StagePtr,
		const Vector3& Position,
		const Vector3& Rotation,
		const Vector3& Scale
		
		) :
		GameObject(StagePtr),
		m_Position(Position),
		m_Rotation(Rotation),
		m_Scale(Scale),
		m_Switch(false),
		onoff(false),
		count(0.0f)			
	{
	}
	Switch::~Switch(){}

	//初期化
	void Switch::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();


		PtrTransform->SetPosition(m_Position);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetScale(m_Scale);
		
		//Rigidbodyをつける
		//auto PtrRedit = AddComponent<Rigidbody>();

		/*//反発を実装する場合はRigidbodyをつける
		//auto PtrRegid = AddComponent<Rigidbody>();*/
		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(false);
		AddComponent<PNTCollisionDraw>();
		auto PtrGravity=AddComponent<Gravity>();
		PtrGravity->SetBaseY(0.0f);
	    
		//auto PtrGravity = AddComponent<Gravity>();
		//PtrGravity->SetBaseY(1.1f);
		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");

		//SEの再生
		auto pSwitchSoundEffect = AddComponent<MultiSoundEffect>();
		pSwitchSoundEffect->AddAudioResource(L"Switch");
		auto pGateSoundEffect = AddComponent<MultiSoundEffect>();
		pGateSoundEffect->AddAudioResource(L"Gate");


		//スイッチの状態に合ったテクスチャを貼り付ける
		SwitchSetTX();
		//スイッチのオン・オフでテクスチャの切り替え
		//スイッチがオフだったら
		
		PtrDraw->SetOwnShadowActive(true);

		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<Switch> >(GetThis<Switch>());
		//最初のステートをDefaultStateに設定
		m_StateMachine->ChangeState(AState::Instance());
	}

	//スイッチチェンジ
	void Switch::SwitchChange()
	{
		if (!m_Switch){
			m_Switch = true;
		}
		else{
			m_Switch = false;
		}
	}

	//スイッチのテクスチャの設定
	void Switch::SwitchSetTX()
	{
		auto PtrDraw = GetComponent<PNTStaticDraw>();
		if (m_Switch){

			PtrDraw->SetTextureResource(L"KIIRO_TX");
		}
		else{
			PtrDraw->SetTextureResource(L"RED_TX");
		}
		
	}
	bool Switch::HitMotion(){
		return true;
		//auto PtrCollsion = GetComponent<CollisionObb>();
		//if (PtrCollsion->GetHitObject())
		//{
		//	auto PlayerHit = dynamic_pointer_cast<Player>(PtrCollsion->GetHitObject());
		//	if (PlayerHit){
		//		auto PtrDraw = GetComponent<PNTStaticDraw>();
		//		//スイッチの切り替え						
		//		if (m_Switch){
		//			m_Switch = false;
		//			PtrDraw->SetTextureResource(L"KIIRO_TX");
		//		}
		//		else{
		//			m_Switch = true;
		//			PtrDraw->SetTextureResource(L"RED_TX");
		//		}
		//	}
		//}
	}

	void Switch::OnUpdate(){
		/*auto PtrTrans = AddComponent<Transform>();
		PtrTrans->SetPosition(m_Position);*/
		//ステートマシンのUpdateを行う
		//この中でステートの更新が行われる(Execute()関数が呼ばれる)

		//スイッチが切り替わった時にインターバルを発生させる
		if (onoff)
		{
			//インターバルのカウント開始
			count += 0.1f;
			if (count > 8.0f)
			{
				onoff = false;
				count = 0.0f;
			}
		}
		m_StateMachine->Update();
		
		

	}
	
	void Switch::OnCollision(const shared_ptr<GameObject>& other)
	{
		//当たったらステートを変更
		//衝突判定
		auto PlayerPtr = dynamic_pointer_cast<Player>(other);

		if (PlayerPtr)
		{
			if (!onoff){

				auto pSwitchSoundEffect = AddComponent<MultiSoundEffect>();
				pSwitchSoundEffect->Start(L"Switch", 0, 1.0f);
				auto pGateSoundEffect = AddComponent<MultiSoundEffect>();
				pGateSoundEffect->Start(L"Gate", 0, 2.0f);

				PlayerHitMotion();
				GetStateMachine()->ChangeState(BState::Instance());
				SwitchChange();
				SwitchSetTX();
				onoff = true;
			}
			/*PlayerHitMotion();
			GetStateMachine()->ChangeState(BState::Instance());
			SwitchChange();
			SwitchSetTX();*/
		}


	}
	//void Switch::SwitchChange(){
	//	auto PtrCollsion = GetComponent<CollisionObb>();
	//	if (PtrCollsion->GetHitObject())
	//	{ 
	//		auto PlayerHit = dynamic_pointer_cast<Player>(PtrCollsion->GetHitObject());
	//		if (PlayerHit){
	//		auto PtrDraw = GetComponent<PNTStaticDraw>();
	//		//スイッチの切り替え
	//			if (m_Switch){
	//				m_Switch = false;
	//				PtrDraw->SetTextureResource(L"KIIRO_TX");
	//			}
	//			else{
	//				m_Switch = true;
	//				PtrDraw->SetTextureResource(L"RED_TX");
	//			}
	//		}
	//	}
	//}
	void Switch::PlayerHitMotion()
	{
		//衝突判定
		/*auto PtrCollison = GetComponent<CollisionObb>();
		if (PtrCollison->GetHitObject())
		{
			auto PlayerPtr = dynamic_pointer_cast<Player>(PtrCollison->GetHitObject());
			if (PlayerPtr)
			{*/
		//相手がプレイヤーだった!?
		//auto PtrGate = GetStage()->GetSharedGameObject<Gate>(L"Gate");
		auto PtrGate = GetStage()->GetSharedGameObject<Gate>(L"Gate");
		if (!m_Switch){
		PostEvent(0, GetThis<Switch>(), PtrGate, L"GateClose");}
		else{
		PostEvent(0, GetThis<Switch>(), PtrGate, L"GateOpen");
}
		//	PostEvent(0.5f, GetThis<Switch>(), PtrGate2, L"SwitchChange");
		

	}
	void Switch::OnLastUpdate(){
		
	}

		//--------------------------------------------------------------------------------------
		//	class AState : public ObjState<Player>;
		//	用途: 通常移動
		//--------------------------------------------------------------------------------------
		//ステートのインスタンス取得
		shared_ptr<AState> AState::Instance(){
			static shared_ptr<AState> instance;
			if (!instance){
				instance = shared_ptr<AState>(new AState);
			}
			return instance;
		}
		//ステートに入ったときに呼ばれる関数
		void AState::Enter(const shared_ptr<Switch>& Obj){
			//何もしない
		}
		//ステート実行中に毎ターン呼ばれる関数
		void AState::Execute(const shared_ptr<Switch>& Obj){

		}
		//ステートにから抜けるときに呼ばれる関数
		void AState::Exit(const shared_ptr<Switch>& Obj){
			//何もしない
		}


		//--------------------------------------------------------------------------------------
		//	class BState : public ObjState<Player>;
		//	用途: ジャンプ状態
		//--------------------------------------------------------------------------------------
		//ステートのインスタンス取得
		shared_ptr<BState> BState::Instance(){
			static shared_ptr<BState> instance;
			if (!instance){
				instance = shared_ptr<BState>(new BState);
			}
			return instance;
		}
		//ステートに入ったときに呼ばれる関数
		void BState::Enter(const shared_ptr<Switch>& Obj){

		}
		//ステート実行中に毎ターン呼ばれる関数
		void BState::Execute(const shared_ptr<Switch>& Obj){
			Obj->PlayerHitMotion();
		}
		//ステートにから抜けるときに呼ばれる関数
		void BState::Exit(const shared_ptr<Switch>& Obj){
			//何もしない
		}


	
}