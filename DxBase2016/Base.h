#pragma once

#include "stdafx.h"

namespace basedx11{

	//--------------------------------------------------------------------------------------
	//	class WhiteBase : public GameObject;
	//	用途: プレイヤーの拠点
	//--------------------------------------------------------------------------------------
	class WhiteBase : public GameObject{
		//ステートマシン
		shared_ptr<StateMachine<WhiteBase>>m_StateMachine;
		Vector3 m_StartPos;
		float m_Span;
	public:
		//構築と破棄
		WhiteBase(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		shared_ptr<StateMachine<WhiteBase> >GetSatateMachine() const{
			return m_StateMachine;
		}
		virtual ~WhiteBase();
		////ホワイトベースの状態
		//bool BaseState();
		//状態を変化
		void StateChange();
		//モーションの実装
		void ObjectHitMotion();
		void ObjectHitMoveMotion();
		//敵と衝突したときの処理
		//virtual void OnCollision(const shared_ptr<GameObject>& other) override;
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnLastUpdate() override;
	};

	//--------------------------------------------------------------------------------------
	//	class BlackBase : public GameObject;
	//	用途: 敵の拠点
	//--------------------------------------------------------------------------------------
	class BlackBase : public GameObject{
		Vector3 m_StartPos;
		float m_Span;
	public:
		//構築と破棄
		BlackBase(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~BlackBase();
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnLastUpdate() override;
	};
}
//endof  basedx11