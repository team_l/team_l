#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	class PurificationPoint : public GameObject;
	//	用途: 浄化ポイントの作成
	//--------------------------------------------------------------------------------------
	//構築と破棄
	PurificationPoint::PurificationPoint(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
		) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	PurificationPoint::~PurificationPoint(){}

	//初期化
	void PurificationPoint::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//反発を実装する場合はRigidbodyをつける
		//auto PtrRegid = AddComponent<Rigidbody>();
		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		//PtrObb->SetFixed(true);
		AddComponent<PNTCollisionDraw>();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(L"AO_TX");
		PtrDraw->SetOwnShadowActive(true);
	}

	void PurificationPoint::OnCollision(const shared_ptr<GameObject>& other)
	{
		//コリジョンを得る
		auto PtrCollision = GetComponent<CollisionObb>();
		auto EnemyPtr = dynamic_pointer_cast<SeekObject>(other);
		if (EnemyPtr)
		{
			//相手のトランスフォームを得る
			auto HitTrans = PtrCollision->GetHitObject()->GetComponent<Transform>();
			auto HitPos = HitTrans->GetPosition();
			//自身のトランスフォームを(ry
			auto PtrTrans = GetComponent<Transform>();
			//自身のポジソンを得る
			auto PtrPos = PtrTrans->GetPosition();

			HitTrans->SetPosition(PtrPos.x, PtrPos.y + 3.75f, PtrPos.z + 2.0f);

		}
		
		
		
	}

	//----------------------------------------------
	//敵の浄化ポイント
	//______________________________________________
	EnemyPurificationPoint::EnemyPurificationPoint(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
		) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	EnemyPurificationPoint::~EnemyPurificationPoint(){}

	void EnemyPurificationPoint::OnCreate()
	{
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//反発を実装する場合はRigidbodyをつける
		//auto PtrRegid = AddComponent<Rigidbody>();
		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		//PtrObb->SetFixed(true);
		//AddComponent<PNTCollisionDraw>();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(L"DIRT_TX");
		PtrDraw->SetOwnShadowActive(true);
	}

	void EnemyPurificationPoint::OnCollision(const shared_ptr<GameObject>& other)
	{
		auto PtrCollision = GetComponent<CollisionObb>();
		auto PlayerPtr = dynamic_pointer_cast<Player>(PtrCollision->GetHitObject());
		if (PlayerPtr)
		{
			//相手のトランスフォームを得る
			auto HitTrans = PtrCollision->GetHitObject()->GetComponent<Transform>();
			auto HitPos = HitTrans->GetPosition();
			//自身のトランスフォームを(ry
			auto PtrTrans = GetComponent<Transform>();
			//自身のポジソンを得る
			auto PtrPos = PtrTrans->GetPosition();

			HitTrans->SetPosition(PtrPos.x, PtrPos.y + 3.75f, PtrPos.z -2.0f);
		}
	}
}